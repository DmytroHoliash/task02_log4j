package com.holiash;

import com.holiash.loggers.CronLogger;
import com.holiash.loggers.DebugLogger;
import com.holiash.loggers.EmailLogger;
import com.holiash.loggers.InfoLogger;
import com.holiash.loggers.SMSLogger;
import com.holiash.loggers.WarnInfoLogger;
import com.holiash.loggers.WarnUpLogger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

  private static Logger logger = LogManager.getLogger(Application.class);

  public static void main(String[] args) {
    logger.debug("debug");
    logger.trace("trace");
    logger.info("info");
    logger.error("error");
    DebugLogger.doDebugLog();
    InfoLogger.doInfoLog();
    CronLogger.doCronLog();
    WarnUpLogger.doWarnUpLog();
    WarnInfoLogger.doWarnInfoLog();
    EmailLogger.doEmailLog();
    SMSLogger.doSMSLog();
  }
}

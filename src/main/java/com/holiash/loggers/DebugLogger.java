package com.holiash.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DebugLogger {

  private static final Logger debugLogger = LogManager.getLogger(DebugLogger.class);

  public static void doDebugLog() {
    debugLogger.trace("trace lig from DebugLogger");
    debugLogger.debug("debug log from DebugLogger");
    debugLogger.info("info log from DebugLogger");
  }
}

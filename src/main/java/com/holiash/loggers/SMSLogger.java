package com.holiash.loggers;

import com.holiash.SMS.SmsAppender;
import com.twilio.twiml.Sms;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SMSLogger {

  private static final Logger smsLogger = LogManager.getLogger(SMSLogger.class);

  public static void doSMSLog() {
    smsLogger.fatal("fatal from " + SMSLogger.class);
  }
}

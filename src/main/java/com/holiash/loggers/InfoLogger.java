package com.holiash.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InfoLogger {

  private static final Logger infoLogger = LogManager.getLogger(InfoLogger.class);

  public static void doInfoLog() {
    infoLogger.trace("trace lig from InfoLogger");
    infoLogger.debug("debug log from InfoLogger");
    infoLogger.info("info log from InfoLogger");
    infoLogger.warn("warn from InfoLogger");
  }
}

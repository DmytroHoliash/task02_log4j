package com.holiash.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WarnUpLogger {

  private static final Logger warnUpLogger = LogManager.getLogger(InfoLogger.class);

  public static void doWarnUpLog() {
    warnUpLogger.trace("trace lig from WarmUpLogger");
    warnUpLogger.debug("debug log from WarmUpLogger");
    warnUpLogger.info("info log from WarmUpLogger");
    warnUpLogger.warn("warn from WarmUpLogger");
    warnUpLogger.error("error log from WarmUpLogger");
    warnUpLogger.fatal("fatal log from WarmUpLogger");
  }
}

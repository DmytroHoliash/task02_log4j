package com.holiash.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CronLogger {

  private static final Logger cronLogger = LogManager.getLogger(CronLogger.class);

  public static void doCronLog() {
    cronLogger.trace("trace lig from CronLogger");
    cronLogger.debug("debug log from CronLogger");
    cronLogger.info("info log from CronLogger");
    cronLogger.warn("warn from CronLogger");
  }
}

package com.holiash.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmailLogger {

  private static final Logger emailLogger = LogManager.getLogger(EmailLogger.class);

  public static void doEmailLog() {
    emailLogger.error("error from " + EmailLogger.class);
  }
}

package com.holiash.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WarnInfoLogger {

  private static final Logger warnInfoLogger = LogManager.getLogger(WarnInfoLogger.class);

  public static void doWarnInfoLog() {
    warnInfoLogger.trace("trace from " + WarnInfoLogger.class);
    warnInfoLogger.debug("debug from " + WarnInfoLogger.class);
    warnInfoLogger.info("info from " + WarnInfoLogger.class);
    warnInfoLogger.warn("warn from " + WarnInfoLogger.class);
    warnInfoLogger.error("error from " + WarnInfoLogger.class);
    warnInfoLogger.fatal("fatal from " + WarnInfoLogger.class);
  }
}
